FROM gcr.io/google-containers/hyperkube:v1.14.6

COPY --from=gcr.io/kubernetes-helm/tiller:v2.14.3 /helm /usr/local/bin/helm
COPY --from=gcr.io/kubernetes-helm/tiller:v2.14.3 /tiller /usr/local/bin/tiller
